var current_course;
var current_tot_course;
var current_tot_cnt;

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
	  var c = ca[i];
	  while (c.charAt(0) == ' ') {
		c = c.substring(1);
	  }
	  if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
	  }
	}
	return "";
  } 

  function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function seleCourse(){
	//載入cookie的值
	 current_course = getCookie(cookie_name/*curr_syear+curr_sem+'_sele_course_'+stu_no*/);
	 current_tot_course;
	 current_tot_cnt;
	 if(current_course == ''){     	
		 current_tot_cnt=0;
	 }else{
		 current_tot_course = current_course.split("|");
		 current_tot_cnt = current_tot_course.length;
	 }
	 if(current_tot_cnt>0){
		 $(".track").each(function(){
			  var course_dept_no = $(this).attr('data-deptno');
			 var course_seq_no = $(this).attr('data-seqno');   
			 var course_dep_seq = course_dept_no + course_seq_no;
			 if(jQuery.inArray( course_dep_seq, current_tot_course )>=0){
				//  $(this).val(recognition_of_credits_cancel);
				 $(this).html(recognition_of_credits_cancel);
				 $(this).css('color',"yellow");
				 $(this).css('font-weight',"bold");
								  
			 }
		 });
	 } 	
}

$(document).ready(function(){
   	//設定已挑選的課程
	seleCourse();

	$('.action-btn').click(function(event) {

		var url = "index.php?c=qry11215";
		var prekey = $(this).data('prekey');
		//執行送出
			$.ajax({
				type : "post",
				url  :  url + "&m=add_presub",
				dataType : "json",
				data : {
						'key' : prekey
					},
				success : function(m) {
					if(m.result!=undefined&&m.result){
						show_success_msg(m.msg);
					}else{
						show_error_msg(m.msg);
					}
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
			        show_error_msg("抱歉，資料處理失敗!");		
		        }
			});
	});
	
	 	//加入/取消徵詢表
		 $(".track").click(function(){
			// console.log($(this));
			var course_dept_no = $(this).attr('data-deptno');
			var course_seq_no = $(this).attr('data-seqno');     		
			var ori_course = getCookie(cookie_name/*curr_syear+curr_sem+'_sele_course_'+stu_no*/);
			var new_course = "";
			
			if(ori_course.indexOf(course_dept_no + course_seq_no)==-1){				     		
				
				if (ori_course == ""){
					new_course = course_dept_no + course_seq_no;
				}else{
					new_course = ori_course+"|"+course_dept_no + course_seq_no;
				}
				
				var tot_course = new_course.split("|");
				
				//已挑選數量
				// $("#track_cnt").html(tot_course.length);
				$(this).html(recognition_of_credits_cancel);
				$(this).css('color',"yellow");
				$(this).css('font-weight',"bold");
		   }
		   
		   else{
			   var kill_str=course_dept_no + course_seq_no;
			   var tot_course = ori_course.split("|");
			   var tot_course_cnt = tot_course.length;
			   //只有一個
			   if(ori_course==kill_str){
				   new_course="";
				   tot_course_cnt=0;
			   }
			   //不只一個
			   else{
				   //只有兩個要刪掉第一個
				   if(tot_course_cnt>=2 && ori_course.indexOf(kill_str)==0){
					   kill_str = kill_str+"|";
				   }
				   //不在第一個
				   else if(ori_course.indexOf(kill_str)>0){
					   kill_str = "|"+kill_str;
				   }
				   new_course = ori_course.replace(kill_str,"");
				   tot_course_cnt = tot_course_cnt-1;
			   }
			//    $("#track_cnt").html(tot_course_cnt);
			   $(this).html(recognition_of_credits_add);
			   $(this).css('color',"white");
			   $(this).css('font-weight',"normal");
			   
		   }
			expire_days = 20; // 過期日期(天)
		   var d = new Date();
		   d.setTime(d.getTime() + (expire_days * 24 * 60 * 60 * 1000));
		   var expires = "expires=" + d.toGMTString();
		   setCookie(cookie_name/*curr_syear+curr_sem+'_sele_course'*/,new_course,365);			
		   current_tot_course = new_course;
		   
		});	

	$('.tr-info').hide();
	$('.btn-see-more').click(function(event) {

		var btnIndex = this.dataset.index;

		$('.tr-info-' + btnIndex).show('slow');
		$('.tr-see-' + btnIndex).hide();
	});

	$('.btn-hide').click(function(event) {

		var btnIndex = this.dataset.index;

		$('.tr-info-' + btnIndex).hide();
		$('.tr-see-' + btnIndex).show('slow');
	});
});