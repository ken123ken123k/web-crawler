#-- coding:UTF-8 --
import numpy as np
import matplotlib.pyplot as plt
import sqlite3
import csv
from random import *
from matplotlib.ticker import FormatStrFormatter
from args import *
import urllib.request as ur
import json
import datetime
from chinese_calendar import is_workday
import ssl
ssl._create_default_https_context =ssl._create_unverified_context
url='https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-079?Authorization=CWB-76BE99B7-7396-4AC6-9369-5E2210A8199D&locationName=%E6%9D%B1%E5%8D%80,%E5%AE%89%E5%B9%B3%E5%8D%80,%E4%B8%AD%E8%A5%BF%E5%8D%80,%E5%8C%97%E5%8D%80,&elementName=Wx&startTime='
site = ur.urlopen(url)
page = site.read()
contents = page.decode()
data = json.loads(contents)
day=datetime.datetime.now().date()
t1 = '8:00'
t2 = '17:00'
time = datetime.datetime.now().strftime("%H:%M")
#北區天氣
weather1 = data['records']['locations'][0]['location'][0]['weatherElement'][0]['time'][0]['elementValue'][0]['value']
#東區天氣
weather2 = data['records']['locations'][0]['location'][1]['weatherElement'][0]['time'][0]['elementValue'][0]['value']
#中西區天氣
weather3 = data['records']['locations'][0]['location'][2]['weatherElement'][0]['time'][0]['elementValue'][0]['value']
#安平區天氣
weather4 = data['records']['locations'][0]['location'][3]['weatherElement'][0]['time'][0]['elementValue'][0]['value']

def readfile():
#path_name="C:/xampp/htdocs/project/web-crawler/tsp2/"
    path_name=""
    args = parse_args()
    if args.file:
        filename = args.file
    elif args.data == 'irent':
        filename = path_name+"data/irent.csv"
    elif args.data == 'nctu':
        filename = path_name+"data/nctu.csv"
    elif args.data == 'nthu':
        filename = path_name+"data/nthu.csv"
    elif args.data == 'thu':
        filename = path_name+"data/thu.csv"
    else:
        print("ERROR: undefined data file")
    return np.loadtxt(filename, delimiter=',',encoding='utf-8-sig')

def utility_value():
    money = (0.255 , 0.069 , -0.324)
    #usage_time = (0.073 , -0.083 , 0.01)
    #weather = (0.937 , 0.173 , -1.110)
    walk_time = (0.222 , 0.022 , -0.243)
    battery = (0.256 , 0.103 , -0.563)
    if weather1=='多雲時晴'or'時晴多雲':
        weather=0.937
    if weather1=='多雲'or'陰天'or'多雲時陰'or'陰時多雲':
        weather=0.173
    if '雨' in weather1:
        weather=-1.110
    #判斷平日通勤、平日非通勤、假日
    if is_workday(day):
        if t1<time<t2:
            usage_time=0.073
        else:
            usage_time=-0.083
    else:
      usage_time=0.01
      
    temp = choice(money) + usage_time +weather+ choice(walk_time)
    total = (round((temp + choice(battery)),3),round(temp + battery[0],3))
    print(usage_time,weather,total)
    return total
    
def randomise(initial):
    unchange_coordinates=np.zeros(shape=(1,2))
    coordinates=np.zeros(shape=(1,2))
    for ini in initial:
        uv = utility_value()
        if uv[0] < 0 and uv[1]-uv[0] > 0.5:
            unchange_coordinates=np.vstack((unchange_coordinates,ini))
        else :
            coordinates=np.vstack((coordinates,ini))
    coordinates=np.delete(coordinates,0,0)
    print(coordinates.shape[0])
    unchange_coordinates=np.delete(unchange_coordinates,0,0)
    print(unchange_coordinates.shape[0])
    return coordinates,unchange_coordinates
    
def export2csv(points):
    filename = 'coordintes'+ '.csv'
    with open(filename, 'a', encoding='utf-8', newline='') as csv_file:
        csv_file.seek(0)
        csv_file.truncate()
        writer = csv.writer(csv_file)
        for point in points:
            writer.writerow(point)
            
def export2csv2(unchange_points):
    filename = 'unchange_coordintes'+ '.csv'
    with open(filename, 'a', encoding='utf-8', newline='') as csv_file:
        csv_file.seek(0)
        csv_file.truncate()
        writer = csv.writer(csv_file)
        for point in unchange_points:
            writer.writerow(point)
            
if __name__ == "__main__":
    initial = readfile()
    coordinates,unchange_coordinates=randomise(initial)
    export2csv(coordinates)
    export2csv2(unchange_coordinates)

